# Use an official Python runtime as a base image
FROM python:3.11

# Set the working directory to /app
WORKDIR /app

# Copy the application files to the working directory
COPY . .

# Install any dependencies needed for your application
# For example, if you have a requirements.txt file:
# COPY requirements.txt .
# RUN pip install --no-cache-dir -r requirements.txt

# Expose the port your application will run on (replace 8000 with your application's port)
EXPOSE 8000

# Command to run the application
CMD ["python", "app.py"]
